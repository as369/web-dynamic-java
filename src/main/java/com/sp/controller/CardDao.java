package com.sp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sp.model.Card;

@Service
public class CardDao {
	private List<Card> myCardList;
	private Random randomGenerator;

	public CardDao() {
		myCardList=new ArrayList<>();
		randomGenerator = new Random();
		createCardList();
	}
	
	private void createCardList() {
		  
		Card c1=new Card("John");
		Card c2=new Card("Roberto");

		myCardList.add(c1);
		myCardList.add(c2);
	}


	public List<Card> getCardList() {
		return this.myCardList;
	}
	
	public Card getCardByName(String name){
		for (Card cardBean : myCardList) {
			if(cardBean.getName().equals(name)){
				return cardBean;
			}
		}
		return null;
	}
	
	public Card getRandomCard(){
		int index=randomGenerator.nextInt(this.myCardList.size());
		return this.myCardList.get(index);
	}

	public Card addCard(String name) {
		Card c=new Card(name);
		this.myCardList.add(c);
		return c;
	}
}

