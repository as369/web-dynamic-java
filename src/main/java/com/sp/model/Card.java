package com.sp.model;

public class Card  {
	private String name;

	public Card() {
		this.name = "";
	}
	
	public Card(String name) {
		this.name = name;
	}


	// GETTER AND SETTER
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
