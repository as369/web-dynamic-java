package com.sp.model;

public class CardFormDTO  {

	private String name;

	public CardFormDTO() {
		this.name = "";
	}
	
	public CardFormDTO(String name) {
		this.name = name;
	}

    // GETTER AND SETTER

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}